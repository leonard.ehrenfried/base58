# base58

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/io.leonard/base58/badge.svg)](https://maven-badges.herokuapp.com/maven-central/io.leonard/base58)

Encode and decode string to an from base58.

The project is deployed to Maven Central and you can include it with

```xml
<dependency>
  <groupId>io.leonard</groupId>
  <artifactId>base58</artifactId>
  <version>${version}</version>
</dependency>
```

or

```
compile 'io.leonard:base58:${version}'
```

## Usage

```java
import io.leonard.Base58
import java.nio.charset.StandardCharsets

//encoding
Base58.encode(uncompressed.getBytes(StandardCharsets.UTF_8));

//decoding
new String(Base58.decode(input), StandardCharsets.UTF_8)

```
