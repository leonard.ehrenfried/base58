release:
	# This is to unlock the gpg-agent before doing the actual signing
	gpg --output /dev/null --sign build.gradle
	./gradlew clean build publish closeAndReleaseRepository
